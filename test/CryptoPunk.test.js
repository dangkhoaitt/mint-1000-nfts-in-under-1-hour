const { ethers } = require("hardhat");
const { expect } = require("chai");
const metadata = require("../build/json/_metadata.json");

const FEE = "50000000000000000";
const baseURI = 'https://gateway.pinata.cloud/ipfs/QmbBJJkRJg3ZnhuQ5We1LLKujscGvQHQ8hN8srw1ntB9Ah/';

describe("CryptoPunk", () => {
  beforeEach(async () => {
    const accounts = await ethers.getSigners();
    admin = accounts[0];
    user1 = accounts[1];

    CryptoPunk = await ethers.getContractFactory("CryptoPunk");
    cryptoPunk = await CryptoPunk.deploy("CryptoPunkToken", "NCP", baseURI);
  });

  describe("baseUri", () => {
    it("set base uri", async() => {
      const baseUri = await cryptoPunk.getBaseURI();
      expect(baseUri).equal(baseURI, "URI not valid");
      console.log('baseUri :>> ', baseUri);
    })

    it("mint", async () => {
      const baseUri = await cryptoPunk.getBaseURI();
      for (let i = 1; i <= JSON.parse(JSON.stringify(metadata)).length; i++) {
        await cryptoPunk.mintToken(
          `Napa CryptoPunk #${i}`,
          `${i}.json`,
          { value: FEE }
        );
        console.log(`Napa CryptoPunk #${i} => ${baseUri}${i}.json`)
      }

      const uri = await cryptoPunk.getTokenURI(1);
      console.log('uri :>> ', uri);
    });
  });
});
