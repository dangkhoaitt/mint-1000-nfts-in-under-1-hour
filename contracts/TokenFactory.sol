//SPDX-License-Identifier: MIT
pragma solidity 0.8.16;

import { ClonesUpgradeable } from "@openzeppelin/contracts-upgradeable/proxy/ClonesUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "./interfaces/ITokenFactory.sol";
import "./CryptoPunk721.sol";
import "./CryptoPunk1155.sol";

contract TokenFactory is ITokenFactory, OwnableUpgradeable {
    address public library721Address;
    address public library1155Address;

    using CountersUpgradeable for CountersUpgradeable.Counter;
    CountersUpgradeable.Counter public lastId;

    /**
     * @dev Check account is controller from account address
     */
    mapping(address => bool) public controllers;

    /**
     * @dev Keep track of token info from created ID
     */
    mapping(uint256 => TokenInfo) public tokenInfos;

    // ============ EVENTS ============

    /// @dev Emit an event when the contract is deployed
    event ContractDeployed(address indexed library721Address, address indexed library1155Address);

    /// @dev Emit an event when token created
    event Create(uint256 indexed id, TokenInfo tokenInfo);

    /// @dev Emit an event when the library721Address is updated
    event SetLibrary721Address(address indexed oldAddress, address indexed newAddress);

    /// @dev Emit an event when the library1155Address is updated
    event SetLibrary1155Address(address indexed oldAddress, address indexed newAddress);
    
    /**
     * @notice Setting states initial when deploy contract and only called once
     * @param _library721Address OSB721 library address
     * @param _library1155Address OSB1155 library address
     */
    function initialize(address _library721Address, address _library1155Address) external initializer {
        require(_library721Address != address(0), "Invalid library721Address");
        require(_library1155Address != address(0), "Invalid library1155Address");
        OwnableUpgradeable.__Ownable_init();
        library721Address = _library721Address;
        library1155Address = _library1155Address;
        emit ContractDeployed(_library721Address, _library1155Address);
    }

    // ============ OWNER-ONLY ADMIN FUNCTIONS =============

    /**
     * @notice Update the new OSB721 library address
     * @param _library721Address Library address
     */
    function setLibrary721Address(address _library721Address) external onlyOwner {
        require(_library721Address != address(0), "Invalid library721Address");
        address oldAddress = library721Address;
        library721Address = _library721Address;
        emit SetLibrary721Address(oldAddress, _library721Address);
    }

    /**
     * @notice Update the new OSB1155 library address
     * @param _library1155Address Library address
     */
    function setLibrary1155Address(address _library1155Address) external onlyOwner {
        require(_library1155Address != address(0), "Invalid library1155Address");
        address oldAddress = library1155Address;
        library1155Address = _library1155Address;
        emit SetLibrary1155Address(oldAddress, _library1155Address);
    }
    
    // ============ PUBLIC FUNCTIONS FOR CREATING =============

    /**
     * @notice Create new a contract with type OSB721 or OSB1155
     * @param _isSingle Token is single
     * @param _baseUri New base URI
     * @param _name Token name
     * @param _symbol Token symbol
     * @param _royaltyReceiver Default royalty receiver address
     * @param _royaltyFeeNumerator Default Percent royalty
     * @return tokenInfo Token info after created
     */
    function create(bool _isSingle, string memory _baseUri, string memory _name, string memory _symbol, address _royaltyReceiver, uint96 _royaltyFeeNumerator) external returns (TokenInfo memory) {
        lastId.increment();
        bytes32 salt = keccak256(abi.encodePacked(lastId.current()));
        address deployedContract;

        if (_isSingle) {
            CryptoPunk721 _cryptoPunk721 = CryptoPunk721(ClonesUpgradeable.cloneDeterministic(library721Address, salt));
            _cryptoPunk721.initialize(_msgSender(), _baseUri, _name, _symbol, _royaltyReceiver, _royaltyFeeNumerator); 
            deployedContract = address(_cryptoPunk721);
        } else {
            CryptoPunk1155 _cryptoPunk1155 = CryptoPunk1155(ClonesUpgradeable.cloneDeterministic(library1155Address, salt));
            _cryptoPunk1155.initialize(_msgSender(), _baseUri, _name, _symbol, _royaltyReceiver, _royaltyFeeNumerator); 
            deployedContract = address(_cryptoPunk1155);
        }

        TokenInfo storage tokenInfo = tokenInfos[lastId.current()];
        tokenInfo.token = deployedContract;
        tokenInfo.owner = _msgSender();
        tokenInfo.receiverRoyaltyFee = _royaltyReceiver;
        tokenInfo.percentageRoyaltyFee = _royaltyFeeNumerator;
        tokenInfo.baseURI = _baseUri;
        tokenInfo.name = _name;
        tokenInfo.symbol = _symbol;
        tokenInfo.isSingle = _isSingle;

        emit Create(lastId.current(), tokenInfo);
        return tokenInfo;
    }
}
