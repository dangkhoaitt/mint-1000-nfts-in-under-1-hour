//SPDX-License-Identifier: MIT
pragma solidity 0.8.16;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/common/ERC2981Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";

contract CryptoPunk721 is ERC721Upgradeable, ERC721EnumerableUpgradeable, ERC2981Upgradeable, OwnableUpgradeable {
    RoyaltyInfo public defaultRoyaltyInfo;

    /// @dev contract address Factory 
    address public factory;

    using StringsUpgradeable for uint256;
    string public baseURI;

    using CountersUpgradeable for CountersUpgradeable.Counter;
    CountersUpgradeable.Counter public lastId;

    /**
     * @dev Check account is controller from account address
     */
    mapping(address => bool) public controllers;

    // ============ EVENTS ============

    /// @dev Emit an event when the contract is deployed
    event ContractDeployed(address indexed owner, string baseUri, string name, string symbol, address indexed receiverRoyaltyFee, uint96 indexed percentageRoyaltyFee);

    /// @dev Emit an event when mintBatch success
    event MintBatch(string indexed oldUri, string indexed newUri, uint256[] tokenIds);

    /// @dev Emit an event when mintWithRoyalty success
    event MintWithRoyalty(address indexed to, address indexed receiverRoyaltyFee, uint96 indexed percentageRoyaltyFee);

    /// @dev Emit an event when MintBatchWithRoyalty success
    event MintBatchWithRoyalty(string indexed oldUri, string indexed newUri, uint256[] tokenIds, address[] receiverRoyaltyFees, uint96[] percentageRoyaltyFees);
    
    /// @dev Emit an event when updated controller
    event SetController(address indexed account, bool allow);
    
    /// @dev Emit an event when updated new base URI
    event SetBaseURI(string indexed oldUri, string indexed newUri);

    /// @dev Emit an event when call multiTransfer success
    event MultiTransfer(address indexed from, address[] to, uint256[] tokenIds);

    /**
     * @notice Setting states initial when deploy contract and only called once
     * @param _owner Contract owner address
     * @param _baseUri Base URI metadata
     * @param _name Token name
     * @param _symbol Token symbol
     * @param _receiverRoyaltyFee Default royalty receiver address
     * @param _percentageRoyaltyFee Default percent royalty
     */
    function initialize(address _owner, string memory _baseUri, string memory _name, string memory _symbol, address _receiverRoyaltyFee, uint96 _percentageRoyaltyFee) public initializer {
        __ERC721_init(_name, _symbol);
        __Ownable_init();
        transferOwnership(_owner);
        
        factory = _msgSender();
        baseURI = _baseUri;

        if (_receiverRoyaltyFee != address(0)) {
            require(_percentageRoyaltyFee > 0, "Invalid percentageRoyaltyFee");
            defaultRoyaltyInfo = RoyaltyInfo(_receiverRoyaltyFee, _percentageRoyaltyFee);
            _setDefaultRoyalty(_receiverRoyaltyFee, _percentageRoyaltyFee);
        }
        emit ContractDeployed(_owner, _baseUri, _name, _symbol, _receiverRoyaltyFee, _percentageRoyaltyFee);
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view override (ERC721Upgradeable, ERC721EnumerableUpgradeable, ERC2981Upgradeable) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    /**
     * @dev Hook that is called before any token transfer. This includes minting
     * and burning.
     *
     * Calling conditions:
     *
     * - When `from` and `to` are both non-zero, ``from``'s `tokenId` will be
     * transferred to `to`.
     * - When `from` is zero, `tokenId` will be minted for `to`.
     * - When `to` is zero, ``from``'s `tokenId` will be burned.
     * - `from` cannot be the zero address.
     * - `to` cannot be the zero address.
     *
     * To learn more about hooks, head to xref:ROOT:extending-contracts.adoc#using-hooks[Using Hooks].
     */
    function _beforeTokenTransfer(address from, address to, uint256 tokenId) internal override (ERC721Upgradeable, ERC721EnumerableUpgradeable) {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    // ============ ACCESS CONTROL/SANITY MODIFIERS ============

    /**
     * @dev To check caller is owner or controller
     */
    modifier onlyOwnerOrController() {
        require(_msgSender() == owner() || controllers[_msgSender()], "Caller is not the owner or controller");
        _;
    }

    // ============ OWNER-ONLY ADMIN FUNCTIONS ============

    /**
     * @notice Delegate controller permission to account
     * @param _account Account that set the controller
     * @param _allow Setting value
     */
    function setController(address _account, bool _allow) external onlyOwner {
        require(_account != address(0), "Invalid account");
        require(controllers[_account] != _allow, "Duplicate setting");
        controllers[_account] = _allow;

        emit SetController(_account, _allow);
    }

    // ============ OWNER OR CONTROLLER-ONLY FUNCTIONS ============

    /**
     * @notice Update new base URI
     * @param _newUri New URI
     */
    function setBaseURI(string memory _newUri) external onlyOwnerOrController {
        string memory oldUri = baseURI;
        baseURI = _newUri;
        emit SetBaseURI(oldUri, _newUri);
    }

    /**
     * @notice Mint a token to an address
     * @param _to Mint to address
     * @return tokenId Token id of owner
     */
    function mint(address _to) external onlyOwnerOrController returns (uint256 tokenId) {
        lastId.increment();
        _safeMint(_to, lastId.current());
        tokenId = lastId.current();
    }

    /**
     * @notice Mint as many tokens to caller
     * @param _baseUri New base URI
     * @param _times Token quantity
     * @return tokenIds The list of owned tokens
     */
    function mintBatch(string memory _baseUri, uint256 _times) external onlyOwnerOrController returns (uint256[] memory tokenIds) {
        require(_times > 0, "Invalid times");
        tokenIds = new uint256[](_times);
        string memory oldUri = baseURI;

        for (uint256 i = 0; i < _times; i++) {
            lastId.increment();
            tokenIds[i] = lastId.current();
            _safeMint(_msgSender(), lastId.current());
        }

        baseURI = _baseUri;
        emit MintBatch(oldUri, _baseUri, tokenIds);
    }  

    /**
     * @notice Mint a token to an address and set royalty
     * @param _to Mint to address
     * @param _receiverRoyaltyFee Royalty receiver address
     * @param _percentageRoyaltyFee Percent royalty
     * @return tokenId Token id of owner
     */
    function mintWithRoyalty(address _to, address _receiverRoyaltyFee, uint96 _percentageRoyaltyFee) external onlyOwnerOrController returns (uint256 tokenId) {
        lastId.increment();
        _safeMint(_to, lastId.current());
        if (_receiverRoyaltyFee == address(0)) _setTokenRoyalty(lastId.current(), defaultRoyaltyInfo.receiver, _percentageRoyaltyFee);
        else _setTokenRoyalty(lastId.current(), _receiverRoyaltyFee, _percentageRoyaltyFee);
        tokenId = lastId.current();
        emit MintWithRoyalty(_to, _receiverRoyaltyFee, _percentageRoyaltyFee);
    }

    /**
     * @notice Mint as many tokens to caller and set royalty
     * @param _baseUri New base URI
     * @param _receiverRoyaltyFees Royalty receiver address per token
     * @param _percentageRoyaltyFees Percent royalty per token
     * @return tokenIds The list of owned tokens
     */
    function mintBatchWithRoyalty(string memory _baseUri, address[] memory _receiverRoyaltyFees, uint96[] memory _percentageRoyaltyFees) external onlyOwnerOrController returns (uint256[] memory tokenIds) {
        require(_receiverRoyaltyFees.length == _percentageRoyaltyFees.length, "Invalid param");
        require(_receiverRoyaltyFees.length > 0, "Invalid receiverRoyaltyFees");

        tokenIds = new uint256[](_percentageRoyaltyFees.length);
        string memory oldUri = baseURI;

        for (uint256 i = 0; i < _receiverRoyaltyFees.length; i++) {
            lastId.increment();
            tokenIds[i] = lastId.current();
            _safeMint(_msgSender(), lastId.current());
            if (_percentageRoyaltyFees[i] == 0) continue;
            if (_receiverRoyaltyFees[i] == address(0)) _setTokenRoyalty(lastId.current(), defaultRoyaltyInfo.receiver, _percentageRoyaltyFees[i]);
            else _setTokenRoyalty(lastId.current(), _receiverRoyaltyFees[i], _percentageRoyaltyFees[i]);
        }
        baseURI = _baseUri;
        emit MintBatchWithRoyalty(oldUri, _baseUri, tokenIds, _receiverRoyaltyFees, _percentageRoyaltyFees);
    }

    // ============ OTHER FUNCTIONS =============

    /**
     * @notice Suport multi transfer
     * @param _accounts Account addresses
     * @param _tokenIds Token IDs
     */
    function multiTransfer(address[] memory _accounts, uint256[] memory _tokenIds) external {
		require(_accounts.length == _tokenIds.length, "tokenIds and accounts length mismatch");

		for (uint256 i = 0; i < _tokenIds.length; i++) {
			safeTransferFrom(_msgSender(), _accounts[i], _tokenIds[i]);
		}

		emit MultiTransfer(_msgSender(), _accounts, _tokenIds);
    }

    /**
     * @notice Takes a tokenId and returns base64 string to represent the token metadata
     * @param _tokenId Id of the token
     * @return string base64
     */
    function tokenURI(uint256 _tokenId) public view virtual override returns (string memory) {
        return bytes(baseURI).length > 0 ? string(abi.encodePacked(baseURI, _tokenId.toString(), ".json")) : ".json";
    }

    /**
     * @notice Returns base64 string to represent the contract metadata
     * See https://docs.opensea.io/docs/contract-level-metadata
     * @return string base64
     */
    function contractURI() public view returns (string memory) {
        return tokenURI(0);
    }
}