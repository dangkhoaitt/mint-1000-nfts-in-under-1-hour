//SPDX-License-Identifier: MIT
pragma solidity 0.8.16;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/common/ERC2981Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";

contract CryptoPunk1155 is ERC1155Upgradeable, ERC2981Upgradeable, OwnableUpgradeable {
    RoyaltyInfo public defaultRoyaltyInfo;

    /// @dev contract address Factory
    address public factory;

    using StringsUpgradeable for uint256;
    string public baseURI;

    using CountersUpgradeable for CountersUpgradeable.Counter;
    CountersUpgradeable.Counter public lastId;

    string public name;
    string public symbol;

    /**
     * @dev Check account is controller from account address
     */
    mapping(address => bool) public controllers;

    // ============ EVENTS ============

    /// @dev Emit an event when the contract is deployed
    event ContractDeployed(address indexed owner, string baseUri, string name, string symbol, address indexed receiverRoyaltyFee, uint96 indexed percentageRoyaltyFee);

    /// @dev Emit an event when mintBatch success
    event MintBatch(string indexed oldUri, string indexed newUri, uint256[] tokenIds, uint256[] amounts);

    /// @dev Emit an event when mintWithRoyalty success
    event MintWithRoyalty(address indexed to, uint256 amount, address indexed receiverRoyaltyFee, uint96 indexed percentageRoyaltyFee);

    /// @dev Emit an event when MintBatchWithRoyalty success
    event MintBatchWithRoyalty(string indexed oldUri, string indexed newUri, uint256[] tokenIds, uint256[] amounts, address[] receiverRoyaltyFees, uint96[] percentageRoyaltyFees);
    
    /// @dev Emit an event when updated controller
    event SetController(address indexed account, bool allow);
    
    /// @dev Emit an event when updated new base URI
    event SetBaseURI(string indexed oldUri, string indexed newUri);

    /// @dev Emit an event when call multiTransfer success
    event MultiTransfer(address indexed from, address[] to, uint256[] tokenIds, uint256[] amounts);

    /**
     * @notice Setting states initial when deploy contract and only called once
     * @param _owner Contract owner address
     * @param _baseUri Base URI metadata
     * @param _name Token name
     * @param _symbol Token symbol
     * @param _receiverRoyaltyFee Default royalty receiver address
     * @param _percentageRoyaltyFee Default Percent royalty
     */
    function initialize(address _owner, string memory _baseUri, string memory _name, string memory _symbol, address _receiverRoyaltyFee, uint96 _percentageRoyaltyFee) public initializer {
        __ERC1155_init("");
        __Ownable_init();
        transferOwnership(_owner);

        factory = _msgSender();
        baseURI = _baseUri;
        name = _name;
        symbol = _symbol;
        
        if (_receiverRoyaltyFee != address(0)) {
            require(_percentageRoyaltyFee > 0, "Invalid percentageRoyaltyFee");
            defaultRoyaltyInfo = RoyaltyInfo(_receiverRoyaltyFee, _percentageRoyaltyFee);
            _setDefaultRoyalty(_receiverRoyaltyFee, _percentageRoyaltyFee);
        }
        emit ContractDeployed(_owner, _baseUri, _name, _symbol, _receiverRoyaltyFee, _percentageRoyaltyFee);
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC1155Upgradeable, ERC2981Upgradeable) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    // ============ ACCESS CONTROL/SANITY MODIFIERS ============

    /**
     * @dev To check caller is owner or controller
     */
    modifier onlyOwnerOrController() {
        require(_msgSender() == owner() || controllers[_msgSender()], "Caller is not the owner or controller");
        _;
    }

    // ============ OWNER-ONLY ADMIN FUNCTIONS ============

    /**
     * @notice Delegate controller permission to account
     * @param _account Account that set the controller
     * @param _allow Setting value
     */
    function setController(address _account, bool _allow) external onlyOwner {
        require(_account != address(0), "Invalid account");
        require(controllers[_account] != _allow, "Duplicate setting");
        controllers[_account] = _allow;

        emit SetController(_account, _allow);
    }

    // ============ OWNER OR CONTROLLER-ONLY FUNCTIONS ============

    /**
     * @notice Update new base URI
     * @param _newUri New URI
     */
    function setBaseURI(string memory _newUri) external onlyOwnerOrController {
        string memory oldUri = baseURI;
        baseURI = _newUri;
        emit SetBaseURI(oldUri, _newUri);
    }

    /**
     * @notice Mint a token to an address
     * @param _to Mint to address
     * @return tokenId Token id of owner
     */
    function mint(address _to, uint256 _amount) external onlyOwnerOrController returns (uint256 tokenId) {
        lastId.increment();
        _mint(_to, lastId.current(), _amount, "");
        tokenId = lastId.current();
    }

    /**
     * @notice Mint as many tokens to caller
     * @param _baseUri New base URI
     * @param _amounts Quantity per token
     * @return tokenIds The list of owned tokens
     */
    function mintBatch(string memory _baseUri, uint256[] memory _amounts) external onlyOwnerOrController returns (uint256[] memory tokenIds) {
        require(_amounts.length > 0, "Invalid amounts");
        tokenIds = new uint256[](_amounts.length);
        string memory oldUri = baseURI;
        for (uint256 i = 0; i < _amounts.length; i++) {
            lastId.increment();
            tokenIds[i] = lastId.current();
            _mint(_msgSender(), lastId.current(), _amounts[i], "");
        }
        baseURI = _baseUri;
        emit MintBatch(oldUri, _baseUri, tokenIds, _amounts);
    }

    /**
     * @notice Mint the token with quantity to an address and set royalty
     * @param _to Mint to address
     * @param _amount Token quantity
     * @param _receiverRoyaltyFee Royalty receiver address
     * @param _percentageRoyaltyFee Percent royalty
     * @return tokenId Token id of owner
     */
    function mintWithRoyalty(address _to, uint256 _amount, address _receiverRoyaltyFee, uint96 _percentageRoyaltyFee) external onlyOwnerOrController returns (uint256 tokenId) {
        lastId.increment();
        _mint(_to, lastId.current(), _amount, "");
        if (_receiverRoyaltyFee == address(0)) _setTokenRoyalty(lastId.current(), defaultRoyaltyInfo.receiver, _percentageRoyaltyFee);
        else _setTokenRoyalty(lastId.current(), _receiverRoyaltyFee, _percentageRoyaltyFee);
        tokenId = lastId.current();
        emit MintWithRoyalty(_to, _amount, _receiverRoyaltyFee, _percentageRoyaltyFee);
    }

    /**
     * @notice Mint as many tokens with quantity to caller and set royalty
     * @param _baseUri New base URI
     * @param _amounts Quantity per token
     * @param _receiverRoyaltyFees Royalty receiver address per token
     * @param _percentageRoyaltyFees Percent royalty per token
     * @return tokenIds The list of owned tokens
     */
    function mintBatchWithRoyalty(string memory _baseUri, uint256[] memory _amounts, address[] memory _receiverRoyaltyFees, uint96[] memory _percentageRoyaltyFees) external onlyOwnerOrController returns (uint256[] memory tokenIds) {
        require(_amounts.length == _receiverRoyaltyFees.length && _receiverRoyaltyFees.length == _percentageRoyaltyFees.length, "Invalid param");
        require(_amounts.length > 0, "Invalid amounts");
        tokenIds = new uint256[](_amounts.length);
        string memory oldUri = baseURI;
        for (uint256 i = 0; i < _amounts.length; i++) {
            lastId.increment();
            tokenIds[i] = lastId.current();
            _mint(_msgSender(), lastId.current(), _amounts[i], "");
            if (_percentageRoyaltyFees[i] == 0) continue;
            if (_receiverRoyaltyFees[i] == address(0)) _setTokenRoyalty(lastId.current(), defaultRoyaltyInfo.receiver, _percentageRoyaltyFees[i]);
            else _setTokenRoyalty(lastId.current(), _receiverRoyaltyFees[i], _percentageRoyaltyFees[i]);
        }
        baseURI = _baseUri;
        emit MintBatchWithRoyalty(oldUri, _baseUri, tokenIds, _amounts, _receiverRoyaltyFees, _percentageRoyaltyFees);
    }

    // ============ OTHER FUNCTIONS =============

    /**
     * @notice Suport multi transfer
     * @param _accounts Account addresses
     * @param _tokenIds Token IDs
     * @param _amounts Token amounts
     */
    function multiTransfer(address[] memory _accounts, uint256[] memory _tokenIds, uint256[] memory _amounts) external {
		require(_tokenIds.length == _accounts.length, "tokenIds and accounts length mismatch");

		for (uint256 i = 0; i < _tokenIds.length; i++) {
			safeTransferFrom(_msgSender(), _accounts[i], _tokenIds[i], _amounts[i], "");
		}

		emit MultiTransfer(_msgSender(), _accounts, _tokenIds, _amounts);
    }   

    /**
     * @notice Takes a tokenId and returns base64 string to represent the token metadata
     * @param _tokenId Id of the token
     * @return string base64
     */
    function uri(uint256 _tokenId) public view override returns (string memory) {
        return bytes(baseURI).length > 0 ? string(abi.encodePacked(baseURI, _tokenId.toString(), ".json")) : ".json";
    }

    /**
     * @notice Returns base64 string to represent the contract metadata
     * See https://docs.opensea.io/docs/contract-level-metadata
     * @return string base64
     */
    function contractURI() public view returns (string memory) {
        return uri(0);
    }
}
