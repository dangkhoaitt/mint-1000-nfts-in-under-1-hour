const { ethers, upgrades } = require("hardhat");
const { blockTimestamp } = require("../utils/utils.js");
require("dotenv").config();
const fs = require("fs");

async function main() {
	//* Get network */
	const network = await ethers.provider.getNetwork();
	const networkName = network.chainId === 31337 ? "hardhat" : network.name;
	const blockTimeNow = await blockTimestamp();

	//* Loading accounts */
	const accounts = await ethers.getSigners();
	const addresses = accounts.map((item) => item.address);
	const deployer = addresses[0];
	let gasPriceTotal = 0;
	
	//* Deploy param */

	//* Loading contract factory */
	const CryptoPunk721 = await ethers.getContractFactory("CryptoPunk721");
	const CryptoPunk1155 = await ethers.getContractFactory("CryptoPunk1155");
	const TokenFactory = await ethers.getContractFactory("TokenFactory");
	const NFTChecker = await ethers.getContractFactory("NFTChecker");

	//* Deploy contracts */
	console.log("================================================================================");
	console.log("DEPLOYING CONTRACTS");
	console.log("================================================================================");
	console.log("chainId   :>> ", network.chainId);
	console.log("chainName :>> ", networkName);
	console.log("deployer  :>> ", deployer);
	console.log("================================================================================");
	const nftChecker = await upgrades.deployProxy(NFTChecker);
	await nftChecker.deployed();
	console.log("NFTChecker             deployed to:>>", nftChecker.address);
	const nftCheckerVerify = await upgrades.erc1967.getImplementationAddress(nftChecker.address);
	console.log("NFTChecker             verify addr:>>", nftCheckerVerify);
	console.log("NFTChecker             gasPrice   :>>", Number(nftChecker.deployTransaction.gasPrice));
	console.log("NFTChecker             gasLimit   :>>", Number(nftChecker.deployTransaction.gasLimit));
	gasPriceTotal += Number(nftChecker.deployTransaction.gasPrice);

	const cryptoPunk721 = await CryptoPunk721.deploy();
	await cryptoPunk721.deployed();
	console.log("CryptoPunk721                 deployed to:>>", cryptoPunk721.address);
	console.log("CryptoPunk721                 gasPrice   :>>", Number(cryptoPunk721.deployTransaction.gasPrice));
	console.log("CryptoPunk721                 gasLimit   :>>", Number(cryptoPunk721.deployTransaction.gasLimit));
	gasPriceTotal += Number(cryptoPunk721.deployTransaction.gasPrice);

	const cryptoPunk1155 = await CryptoPunk1155.deploy();
	await cryptoPunk1155.deployed();
	console.log("CryptoPunk1155                deployed to:>>", cryptoPunk1155.address);
	console.log("CryptoPunk1155                gasPrice   :>>", Number(cryptoPunk1155.deployTransaction.gasPrice));
	console.log("CryptoPunk1155                gasLimit   :>>", Number(cryptoPunk1155.deployTransaction.gasLimit));
	gasPriceTotal += Number(cryptoPunk1155.deployTransaction.gasPrice);

	const tokenFactory = await upgrades.deployProxy(TokenFactory, [cryptoPunk721.address, cryptoPunk1155.address]);
	await tokenFactory.deployed();
	console.log("TokenFactory             deployed to:>>", tokenFactory.address);
	const tokenFactoryVerify = await upgrades.erc1967.getImplementationAddress(tokenFactory.address);
	console.log("TokenFactory             verify addr:>>", tokenFactoryVerify);
	console.log("TokenFactory             gasPrice   :>>", Number(tokenFactory.deployTransaction.gasPrice));
	console.log("TokenFactory             gasLimit   :>>", Number(tokenFactory.deployTransaction.gasLimit));
	gasPriceTotal += Number(tokenFactory.deployTransaction.gasPrice);

	console.log("================================================================================");
	console.log("gasPriceTotal :>> ", gasPriceTotal);
	console.log("================================================================================");
	console.log("DONE");
	console.log("================================================================================");

	const verifyArguments = {
		chainId: network.chainId,
		deployer: deployer,
		nftChecker: nftChecker.address,
		nftCheckerVerify: nftCheckerVerify,
		cryptoPunk721: cryptoPunk721.address,
		cryptoPunk1155: cryptoPunk1155.address,
		tokenFactory: tokenFactory.address,
		tokenFactoryVerify: tokenFactoryVerify,
	};

	const dir = `./deploy-history/${network.chainId}-${networkName}/`;
	const fileName = network.chainId === 31337 ? "hardhat" : blockTimeNow;
	if (!fs.existsSync(dir)) fs.mkdirSync(dir, { recursive: true });
	await fs.writeFileSync("contracts.json", JSON.stringify(verifyArguments));
	await fs.writeFileSync(`${dir}/${fileName}.json`, JSON.stringify(verifyArguments));
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
	.then(() => process.exit(0))
	.catch((error) => {
		console.error(error);
		process.exit(1);
	});
